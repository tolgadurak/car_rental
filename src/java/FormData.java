import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.event.ActionEvent;

@ManagedBean
@RequestScoped
public class FormData {

    private String pickUpDate, pickUpLocation;
    private String dropOffDate, dropOffLocation;
    private String vehicleType;
    private boolean locationsAreSame;

    public FormData() {
    }

    public String getPickUpDate() {
        return pickUpDate;
    }

    public void setPickUpDate(String pickUpDate) {
        this.pickUpDate = pickUpDate;
    }

    public String getPickUpLocation() {
        return pickUpLocation;
    }

    public void setPickUpLocation(String pickUpLocation) {
        this.pickUpLocation = pickUpLocation;
    }

    public String getDropOffDate() {
        return dropOffDate;
    }

    public void setDropOffDate(String dropOffDate) {
        this.dropOffDate = dropOffDate;
    }

    public String getDropOffLocation() {
        return dropOffLocation;
    }

    public void setDropOffLocation(String dropOffLocation) {
        this.dropOffLocation = dropOffLocation;
    }
    
    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }
    
    public boolean isLocationsAreSame() {
        return locationsAreSame;
    }

    public void setLocationsAreSame(boolean locationsAreSame) {
        this.locationsAreSame = locationsAreSame;
    }

    public void showResult(ActionEvent e) {
        if (locationsAreSame) dropOffLocation = pickUpLocation;
        System.out.println(pickUpDate + pickUpLocation + dropOffDate + dropOffLocation);
        System.out.println(vehicleType);
    }

}
