import java.io.IOException;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import jsf.util.JsfUtil;

@ManagedBean(eager=true)
@ApplicationScoped
public class UserPreferences {

    private String theme;

    public UserPreferences() throws IOException {
        theme = "public";
    }
    
    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }
    
    public String updateTheme() {
        JsfUtil.addSuccessMessage("Site teması " + theme + " olarak güncellendi.");
        return "settings";
    }
        
    public String getThemeLocation() throws IOException {
        return "/layouts/" + theme + ".xhtml";
    }

    public boolean isAdvancedTheme() {
        return !theme.equals("public");
    }

}