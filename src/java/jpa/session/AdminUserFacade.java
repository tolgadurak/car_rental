/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa.session;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import jpa.entities.AdminUser;

/**
 *
 * @author doga
 */
@Stateless
public class AdminUserFacade extends AbstractFacade<AdminUser> {
    @PersistenceContext(unitName = "CarRentalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AdminUserFacade() {
        super(AdminUser.class);
    }
    
    public List<AdminUser> findByUsernamePassword(AdminUser adminUser) {
        return (List<AdminUser>) em.createNamedQuery("AdminUser.findByUsernamePassword")
                .setParameter("username", adminUser.getUsername())
                .setParameter("password", adminUser.getPassword())
                .getResultList();
    }

}
