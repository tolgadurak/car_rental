/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa.session;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import jpa.entities.Booking;

/**
 *
 * @author Tolga
 */
@Stateless
public class BookingFacade extends AbstractFacade<Booking> {
    @PersistenceContext(unitName = "CarRentalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public BookingFacade() {
        super(Booking.class);
    }
    
    public List<Booking> findByVehicle(Booking booking) {
        return (List<Booking>) em.createNamedQuery("Booking.findByVehicleId")
                .setParameter("vehicleId", booking.getVehicleId())
                .getResultList();
    }
    
}
