/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author engin
 */
@Entity
@Table(name = "VEHICLE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vehicle.findAll", query = "SELECT v FROM Vehicle v"),
    @NamedQuery(name = "Vehicle.findById", query = "SELECT v FROM Vehicle v WHERE v.id = :id"),
    @NamedQuery(name = "Vehicle.findByTitle", query = "SELECT v FROM Vehicle v WHERE v.title = :title"),
    @NamedQuery(name = "Vehicle.findByCategory", query = "SELECT v FROM Vehicle v WHERE v.category = :category"),
    @NamedQuery(name = "Vehicle.findByBrand", query = "SELECT v FROM Vehicle v WHERE v.brand = :brand"),
    @NamedQuery(name = "Vehicle.findByModel", query = "SELECT v FROM Vehicle v WHERE v.model = :model"),
    @NamedQuery(name = "Vehicle.findByPrice", query = "SELECT v FROM Vehicle v WHERE v.price = :price"),
    @NamedQuery(name = "Vehicle.findByPlate", query = "SELECT v FROM Vehicle v WHERE v.plate = :plate"),
    @NamedQuery(name = "Vehicle.findByVyear", query = "SELECT v FROM Vehicle v WHERE v.vyear = :vyear"),
    @NamedQuery(name = "Vehicle.findByColor", query = "SELECT v FROM Vehicle v WHERE v.color = :color"),
    @NamedQuery(name = "Vehicle.findByFuel", query = "SELECT v FROM Vehicle v WHERE v.fuel = :fuel"),
    @NamedQuery(name = "Vehicle.findByGear", query = "SELECT v FROM Vehicle v WHERE v.gear = :gear"),
    @NamedQuery(name = "Vehicle.findByEngineVol", query = "SELECT v FROM Vehicle v WHERE v.engineVol = :engineVol"),
    @NamedQuery(name = "Vehicle.findByImages", query = "SELECT v FROM Vehicle v WHERE v.images = :images")})
public class Vehicle implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "TITLE")
    private String title;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CATEGORY")
    private short category;
    @Size(max = 20)
    @Column(name = "BRAND")
    private String brand;
    @Size(max = 20)
    @Column(name = "MODEL")
    private String model;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PRICE")
    private int price;
    @Size(max = 20)
    @Column(name = "PLATE")
    private String plate;
    @Column(name = "VYEAR")
    private Short vyear;
    @Size(max = 20)
    @Column(name = "COLOR")
    private String color;
    @Column(name = "FUEL")
    private Short fuel;
    @Column(name = "GEAR")
    private Short gear;
    @Column(name = "ENGINE_VOL")
    private Short engineVol;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "IMAGES")
    private String images;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "vehicleId")
    private Collection<Booking> bookingCollection;

    public Vehicle() {
    }

    public Vehicle(Integer id) {
        this.id = id;
    }

    public Vehicle(Integer id, String title, short category, int price, String images) {
        this.id = id;
        this.title = title;
        this.category = category;
        this.price = price;
        this.images = images;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public short getCategory() {
        return category;
    }

    public void setCategory(short category) {
        this.category = category;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public Short getVyear() {
        return vyear;
    }

    public void setVyear(Short vyear) {
        this.vyear = vyear;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Short getFuel() {
        return fuel;
    }

    public void setFuel(Short fuel) {
        this.fuel = fuel;
    }

    public Short getGear() {
        return gear;
    }

    public void setGear(Short gear) {
        this.gear = gear;
    }

    public Short getEngineVol() {
        return engineVol;
    }

    public void setEngineVol(Short engineVol) {
        this.engineVol = engineVol;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vehicle)) {
            return false;
        }
        Vehicle other = (Vehicle) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return id + " - " + title;
    }

    @XmlTransient
    public Collection<Booking> getBookingCollection() {
        return bookingCollection;
    }

    public void setBookingCollection(Collection<Booking> bookingCollection) {
        this.bookingCollection = bookingCollection;
    }
    
}
