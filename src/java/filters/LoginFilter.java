/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filters;

import java.io.IOException;
import javax.enterprise.context.Dependent;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import jsf.AdminUserController;
import org.jboss.weld.servlet.SessionHolder;

/**
 *
 * @author Tolga
 */
@Named(value = "loginFilter")
@Dependent

public class LoginFilter implements Filter {

    /**
     * Creates a new instance of LoginFilter
     */
    public LoginFilter() {

    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        AdminUserController auth = (AdminUserController) req.getSession().getAttribute("adminUserController");
        String reqURI = req.getRequestURI();
        System.out.println(reqURI);
        if (auth == null || !auth.isLoggedIn()) {
            if (reqURI.indexOf("/faces/admin/index.xhtml") >= 0 || reqURI.contains("/faces/admin/booking/")
                    || reqURI.contains("/faces/admin/client/") || reqURI.contains("/faces/admin/vehicle/") 
                    ||reqURI.contains("faces/admin/adminUser")||reqURI.contains("faces/admin/settings") 
                    ||reqURI.contains("faces/admin/application") || reqURI.contains("faces/admin/campaign")) {
                res.sendRedirect(req.getServletContext().getContextPath() + "/faces/admin/login.xhtml");
            } else {
                chain.doFilter(request, response);
            }
        } else {
            if (reqURI.indexOf("/faces/admin/login.xhtml") >= 0) {
                res.sendRedirect(req.getServletContext().getContextPath() + "/faces/admin/index.xhtml");
            } else if (reqURI.indexOf("/faces/admin/logout.xhtml") >= 0) {
                req.getSession().removeAttribute("adminUserController");
                res.sendRedirect(req.getServletContext().getContextPath() + "/faces/admin/login.xhtml");

            } else {
                chain.doFilter(request, response);
            }
        }

    }

    @Override
    public void destroy() {

    }

}
