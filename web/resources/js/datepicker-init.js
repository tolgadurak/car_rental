$(function() {
    $("#searchForm\\:pickupDate").datepicker({
        showOn: "button",
        buttonImage: "resources/images/datePicker/calendar-icon.gif",
        buttonImageOnly: true,
        dateFormat: "yy-mm-dd",
        monthNames: ["Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"],
        dayNamesMin: ["Pts", "Sl", "Çrş", "Prş", "Cm", "Cts", "Pzr"]});
    $("#searchForm\\:dropOffDate").datepicker({
        showOn: "button",
        buttonImage: "resources/images/datePicker/calendar-icon.gif",
        buttonImageOnly: true,
        dateFormat: "yy-mm-dd",
        monthNames: ["Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"],
        dayNamesMin: ["Pts", "Sl", "Çrş", "Prş", "Cm", "Cts", "Pzr"]});

});