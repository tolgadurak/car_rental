INSERT INTO vehicle (title, category, brand, model, price, plate, vyear, color, fuel, gear, engine_vol, images)
VALUES ('Fiat Linea 1.3 Multijet', 1, 'Fiat', 'Linea', 80, '34 AA 0002', 2012, 'Beyaz', 2, 1, 1300, 'vehicles/fiatlinea');

INSERT INTO vehicle (title, category, brand, model, price, plate, vyear, color, fuel, gear, engine_vol, images)
VALUES ('Audi A4 2.0 TDI 2009', 2, 'Audi', 'A4', 150, '34 AA 0003', 2009, 'Siyah', 2, 2, 2000, 'vehicles/audia4');

INSERT INTO vehicle (title, category, brand, model, price, plate, vyear, color, fuel, gear, engine_vol, images)
VALUES ('Opel Astra 1.3 CDTI Sport', 1, 'Opel', 'Astra', 120, '34 AA 0004', 2014, 'Beyaz', 2, 1, 1600, 'vehicles/opelastra');

INSERT INTO vehicle (title, category, brand, model, price, plate, vyear, color, fuel, gear, engine_vol, images)
VALUES ('Volkwagen Golf 1.6 FSI Goal 2007', 1, 'Volkswagen', 'Golf', 100, '34 AA 0005', 2007, 'Beyaz', 1, 2, 1600, 'vehicles/volkswagengolf');

INSERT INTO vehicle (title, category, brand, model, price, plate, vyear, color, fuel, gear, engine_vol, images)
VALUES ('Toyota Corolla 1.4D-4D 2008', 1, 'Toyota', 'Corolla', 100, '34 AA 0006', 2008, 'Gri', 2, 1, 2000, 'vehicles/toyotacorolla');

INSERT INTO vehicle (title, category, brand, model, price, plate, vyear, color, fuel, gear, engine_vol, images)
VALUES ('Lamborghini Gallardo LP560-4', 4, 'Lamborghini', 'Gallardo', 3000, '54 AA 0001', 2013, 'Siyah', 1, 1, 5500, 'vehicles/lamborghini');

INSERT INTO vehicle (title, category, brand, model, price, plate, vyear, color, fuel, gear, engine_vol, images)
VALUES ('Mercedes-Benz C 180 2014', 4, 'Mercedes-Benz', 'C 180', 200, '34 AA 0007', 2014, 'Beyaz', 1, 2, 2000, 'vehicles/mercedesbenzc180');

INSERT INTO vehicle (title, category, brand, model, price, plate, vyear, color, fuel, gear, engine_vol, images)
VALUES ('Range Rover Sport 3.0 SDV6 Premium HSE', 3, 'Land Rover', 'Range Rover Sport', 300, '34 AA 0008', 2013, 'Siyah', 2, 2, 3000, 'vehicles/rangerover');

INSERT INTO vehicle (title, category, brand, model, price, plate, vyear, color, fuel, gear, engine_vol, images)
VALUES ('Porsche Cayenne 3.0 TDI 2013', 3, 'Porsche', 'Cayenne', 400, '34 AA 0009', 2013, 'Beyaz', 2, 1, 3000, 'vehicles/porsche');

INSERT INTO vehicle (title, category, brand, model, price, plate, vyear, color, fuel, gear, engine_vol, images)
VALUES ('Mercedes-Benz Vito 113 CDI 2012', 5, 'Mercedes-Benz', 'Vito', 200, '34 AA 0010', 2012, 'Gri', 2, 1, 2500, 'vehicles/mercedesvito');

INSERT INTO CARRENTAL.CLIENT (FIRSTNAME, LASTNAME, BIRTHDATE, GENDER, PHONE, ADDRESS, EMAIL, PASSWORD) 
	VALUES ('Engin', 'ISIK', '1993-10-26', 1, '05350858531', 'Turgutlu köyü/Pamukova-Sakarya', 'iskengin@gmail.com', '12345');

INSERT INTO CARRENTAL.CLIENT (FIRSTNAME, LASTNAME, BIRTHDATE, GENDER, PHONE, ADDRESS, EMAIL, PASSWORD) 
	VALUES ('Olcay', 'SAHAN', '1988-01-15', 1, '05556667789', 'Alibey Sok. Kemal Mah./Beşiktaş-Istanbul', 'olcaysahan@gmail.com', '12444');

INSERT INTO CARRENTAL.CLIENT (FIRSTNAME, LASTNAME, BIRTHDATE, GENDER, PHONE, ADDRESS, EMAIL, PASSWORD) 
	VALUES ('Pascal', 'NOUMA', '1979-01-01', 1, '05445556677', 'Sarısakık sok. Sebzeliköy Mah./Bağcılar-Istanbul', 'noumabjk@gmail.com', '54231');

INSERT INTO CARRENTAL.CLIENT (FIRSTNAME, LASTNAME, BIRTHDATE, GENDER, PHONE, ADDRESS, EMAIL, PASSWORD) 
	VALUES ('Irem', 'KIRGIZ', '1980-11-14', 2, '05424445643', 'Sürmeli Sok. Dar Mah./Urla-Izmır', 'iremkırgız@gmail.com', 'asfasga');

INSERT INTO CARRENTAL.CLIENT (FIRSTNAME, LASTNAME, BIRTHDATE, GENDER, PHONE, ADDRESS, EMAIL, PASSWORD) 
	VALUES ('Ceyda', 'ATES', '1990-06-25', 2, '05354567634', 'Yesilırmak Sok. Cihangir Mah. /Avcılar-Istanbul', 'atesliceyda@hotmail.com', 'benceyda');

INSERT INTO CARRENTAL.CLIENT (FIRSTNAME, LASTNAME, BIRTHDATE, GENDER, PHONE, ADDRESS, EMAIL, PASSWORD) 
	VALUES ('Doga', 'SEZGIN', '1993-11-08', 2, '05535829690', 'Arıkoy Mah./Sariyer-Istanbul', 'dogasezgin@gmail.com', 'portakal2');

INSERT INTO CARRENTAL.CLIENT (FIRSTNAME, LASTNAME, BIRTHDATE, GENDER, PHONE, ADDRESS, EMAIL, PASSWORD) 
	VALUES ('Mustafa', 'HASTURK', '1992-03-28', 2, '05418524241', 'Gumuspala Mah/Avcılar-Istanbul', 'mustafahasturk@yandex.com', 'bennebileyimyandexmiyim');

INSERT INTO CARRENTAL.CLIENT (FIRSTNAME, LASTNAME, BIRTHDATE, GENDER, PHONE, ADDRESS, EMAIL, PASSWORD) 
	VALUES ('Tolga', 'DURAK', '1993-06-25', 1, '05333333333', 'Torium arkası/Esenyurt-Istanbul', 'tolgatolga@gmail.com', 'tolgatolga2');

INSERT INTO CARRENTAL.CLIENT (FIRSTNAME, LASTNAME, BIRTHDATE, GENDER, PHONE, ADDRESS, EMAIL, PASSWORD) 
	VALUES ('Serenay', 'SEVIM', '1993-05-03', 2, '05350434344', 'Arkasokaklar Sok./Arnavutkoy-Istanbul', 'rızabaskomser@gmail.com', '214322');

INSERT INTO CARRENTAL.CLIENT (FIRSTNAME, LASTNAME, BIRTHDATE, GENDER, PHONE, ADDRESS, EMAIL, PASSWORD) 
	VALUES ('Berna', 'ISIL', '1993-12-12', 2, '05364566544', 'Uzun Sok. Kitapcılar Mah/ Golcuk- Izmir', 'berna@gmail.com', '9999888');

INSERT INTO CARRENTAL.CAMPAIGN (TITLE, DESCRIPTION, BEGIN_DATE, END_DATE) 
	VALUES ('Petrol Ofislerinde %6 ya varan indirim!', 'IU Car Rental den araç kiralayan müşterilerimiz, 25 Kasım tarihine kadar petrol ofislerinden alacağı yakıtlarında %6 indirim kazanacaklar.', '2014-05-30', '2014-11-25');

INSERT INTO CARRENTAL.CAMPAIGN (TITLE, DESCRIPTION, BEGIN_DATE, END_DATE) 
	VALUES ('15 Yıllık sürücülere müjde!', '15 Yılı geçkin ehliyet sahipleri! IU Car Rentalden kiralayacağınız araçlar 1 aylığına %50 indirimli', '2014-05-30', '2014-06-30');

INSERT INTO CARRENTAL.CAMPAIGN (TITLE, DESCRIPTION, BEGIN_DATE, END_DATE) 
	VALUES ('Az kilometre Az Para', 'IU Car Rentalden kiraladığınız araçlarınızda 100 km den daha az mesafe yol katleden müşterilerimiz için araçlar %50 indirimli.', '2014-05-30', '2015-05-30');

INSERT INTO CARRENTAL.CAMPAIGN (TITLE, DESCRIPTION, BEGIN_DATE, END_DATE) 
	VALUES ('Müşterisini Düşünen Car Rental!', 'IU Car Rental olarak, bugüne kadar 10 araç kiralamış müşterilerimize,artık %25 indirim yapıyoruz', '2014-05-30', '2015-05-30');

INSERT INTO booking (client_id, vehicle_id, pickup_date, pickup_location, dropoff_date, dropff_location, total_price, description)
VALUES(8, 1, '2014-05-30', 'Bursa', '2014-06-05', 'Bursa', 1200, 'Aracın deposu full olarak mı verilecek? Teşekkürler');

INSERT INTO booking (client_id, vehicle_id, pickup_date, pickup_location, dropoff_date, dropff_location, total_price, description)
VALUES(9, 2, '2014-05-30', 'İstanbul', '2014-06-05', 'İstanbul', 2000, 'Aracın deposu full olarak mı verilecek? Teşekkürler');

INSERT INTO booking (client_id, vehicle_id, pickup_date, pickup_location, dropoff_date, dropff_location, total_price, description)
VALUES(10, 3, '2014-05-30', 'Bursa', '2014-06-03', 'Bursa', 2000, 'Aracınızda hava yastığı mevcut mu?');

INSERT INTO booking (client_id, vehicle_id, pickup_date, pickup_location, dropoff_date, dropff_location, total_price, description)
VALUES(3, 4, '2014-05-30', 'Sakarya', '2014-06-08', 'Sakarya', 3000, 'Çok havalı görünüyor. 0-100 kmsi kaç bunun?');

INSERT INTO booking (client_id, vehicle_id, pickup_date, pickup_location, dropoff_date, dropff_location, total_price, description)
VALUES(5, 2, '2014-05-30', 'İstanbul', '2050-06-08', 'İstanbul', 3000, 'Aracı kaça satarsınız?');

INSERT INTO application(firstname, lastname, phone, email, description) VALUES('Mustafa', 'Hastürk', '123456789', 'mustafa.hasturk@yandex.com', 'Wolkwagen Golf 1.6 FSI Aracını 2014-05-30 / 2014-06-05 tarihlerini arasında kiralamak istiyorum.');
INSERT INTO application(firstname, lastname, phone, email, description) VALUES('Tolga', 'Durak', '123456789', 'tdurak93@gmail.com', 'Opel Astra 1.3 CDTI Sport Aracını 2014-05-30 / 2014-06-05 tarihlerini arasında kiralamak istiyorum.');
INSERT INTO application(firstname, lastname, phone, email, description) VALUES('Engin', 'Işık', '123456789', 'iskengin@gmail.com', 'Opel Astra 1.3 CDTI Sport Aracını 2014-05-30 / 2014-06-05 tarihlerini arasında kiralamak istiyorum.');
INSERT INTO application(firstname, lastname, phone, email, description) VALUES('Doğa', 'Sezgin', '123456789', 'doga.sezgin@gmail.com', 'Porsche Cayenne 3.0 TDI Aracını 2014-05-30 / 2014-06-10 tarihlerini arasında kiralamak istiyorum.');
INSERT INTO application(firstname, lastname, phone, email, description) VALUES('Gizem', 'Saygın', '123456789', 'ggizem_sygn@gmail.com', 'Porsche Cayenne 3.0 TDI Aracını 2014-05-30 / 2014-06-10 tarihlerini arasında kiralamak istiyorum.');

INSERT INTO vehicle (title, category, brand, model, price, plate, vyear, color, fuel, gear, engine_vol, images)
VALUES ('Hyundai i30 1.6 CRDi Prime OTM', 1, 'Hyundai', 'i30', 100, '34 AA 0010', 2009, 'Kahverengi', 2, 1, 1600, 'vehicles/mercedesvito');

INSERT INTO vehicle (title, category, brand, model, price, plate, vyear, color, fuel, gear, engine_vol, images)
VALUES ('BMW X6 M M', 3, 'BMW', 'X6', 250, '34 AA 0010', 2009, 'Kırmızı', 1, 2, 4395, 'vehicles/bmwx6mm');

INSERT INTO vehicle (title, category, brand, model, price, plate, vyear, color, fuel, gear, engine_vol, images)
VALUES ('Porsche Cayenne Turbo S Tiptronic', 3, 'Porsche', 'Cayenne', 250, '34 AA 0010', 2008, 'Kırmızı', 1, 2, 4806, 'vehicles/porscheCayenneTurbos');
