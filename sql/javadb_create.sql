/* 
	Veritabanını bastan olusturmak icin gerekli sql.
*/

CREATE TABLE client (
    id INTEGER NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    firstname VARCHAR(20) NOT NULL,
    lastname VARCHAR(20) NOT NULL,
    birthdate DATE NOT NULL,
    gender SMALLINT NOT NULL,
    phone VARCHAR(20) NOT NULL,
    address LONG VARCHAR,
    email VARCHAR(40) NOT NULL,
    password VARCHAR(40) NOT NULL
);

CREATE TABLE vehicle (
    id INTEGER NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    title VARCHAR(40) NOT NULL,
    category SMALLINT NOT NULL,
    brand VARCHAR(20),
    model VARCHAR(20),
    price INTEGER NOT NULL,
    plate VARCHAR(20),
    vyear SMALLINT,
    color VARCHAR(20),
    fuel SMALLINT,
    gear SMALLINT,
    engine_vol SMALLINT,
    images VARCHAR(60) NOT NULL
);

CREATE TABLE booking (
    id INTEGER NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    client_id INTEGER NOT NULL,
    vehicle_id INTEGER NOT NULL,
    pickup_date DATE,
    pickup_location VARCHAR(40),
    dropoff_date DATE,
    dropff_location VARCHAR(40),
    total_price INTEGER,
    description LONG VARCHAR
);

CREATE TABLE admin_user (
    id INTEGER NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    username VARCHAR(40) NOT NULL,
    password VARCHAR(40) NOT NULL
);

CREATE TABLE application (
	id INTEGER NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
	firstname VARCHAR(20) NOT NULL,
	lastname VARCHAR(20) NOT NULL,
	phone VARCHAR(20) NOT NULL,
	email VARCHAR(40),
	description LONG VARCHAR
);

CREATE TABLE campaign (
	id INTEGER NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
	title VARCHAR(40) NOT NULL,
	description LONG VARCHAR NOT NULL,
	begin_date DATE,
	end_date DATE
);

ALTER TABLE booking ADD CONSTRAINT booking_fk_client FOREIGN KEY (client_id) REFERENCES client(id);
ALTER TABLE booking ADD CONSTRAINT booking_fk_vehicle FOREIGN KEY (vehicle_id) REFERENCES vehicle(id);

INSERT INTO admin_user (username, password) VALUES ('demo','demo');
